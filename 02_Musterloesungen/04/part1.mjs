// Importieren des fs-Moduls für Dateisystemoperationen
import fs from "node:fs";

// Lesen des Inhalts der Datei 'input.txt' synchron als UTF-8-codierter Text
const lines = fs
  .readFileSync("input.txt", { encoding: "utf-8" })
  // Aufteilen des gelesenen Textes in ein Array von Zeilen
  .split("\n")
  // Konvertieren jeder Zeile in eine Ganzzahl
  .map(x => parseInt(x));

// Durchlaufen des Arrays mit einer doppelten Schleife, um Paare von Zahlen zu finden
for (let i = 0; i < lines.length; i++) {
  for (let j = i + 1; j < lines.length; j++) {
    // Überprüfen, ob die Summe des aktuellen Paars gleich 2020 ist
    if (lines[i] + lines[j] == 2020) {
      // Wenn ja, Ausgabe der beiden Zahlen und ihres Produkts
      console.log(lines[i], lines[j], lines[i] * lines[j]);
    }
  }
}

