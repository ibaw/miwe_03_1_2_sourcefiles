const randomNumber = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

const luckyNumber = randomNumber(0, 10);

export default (guess) => (guess === luckyNumber ? true : false);
