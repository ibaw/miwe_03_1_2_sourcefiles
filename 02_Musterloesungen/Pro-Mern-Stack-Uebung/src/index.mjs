import express from "express";

const app = express();

app.use(express.static("public"));

app.get("/test", (req, res) => {
  res.send("hello world");
});

app.listen(3000, () => {
  console.log("app started");
});
