// users.js

// Ein Array, um Benutzer zu speichern
let users = [];

// Funktion, um einen Benutzer hinzuzufügen
export function addUser(name) {
  users.push(name);
  console.log(`Benutzer hinzugefügt: "${name}"`);
}

// Funktion, um die aktuelle Benutzerliste anzuzeigen
export function listUsers() {
  console.log('Aktuelle Benutzerliste:');
  users.forEach((user, index) => {
    console.log(`${index + 1}. ${user}`);
  });
}

// Funktion, um einen Benutzer zu entfernen (nach Index)
export function removeUser(index) {
  if (index >= 0 && index < users.length) {
    const removed = users.splice(index, 1);
    console.log(`Benutzer entfernt: "${removed[0]}"`);
  } else {
    console.log('Ungültiger Index.');
  }
}
