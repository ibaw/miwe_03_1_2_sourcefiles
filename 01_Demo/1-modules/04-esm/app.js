// app.js

import { addUser, listUsers, removeUser } from './users.js';

// Arbeitsfluss für das Benutzerverzeichnis
addUser('Alice');
addUser('Bob');
listUsers();
removeUser(0);
listUsers();
