import express from 'express';

// Es gibt extra eine ESM Version
import { shuffle } from 'lodash-es';


// Express.JS Instanz erstellen
const app = express();

// Port aus .env File auslesen (best practice)
// Wenn keine Angabe, default Port 3000 verwenden
const PORT = process.env.PORT || 3000;

// Beispielhafte Nutzung von Lodash
const numbers = [1, 2, 3, 4, 5];
const shuffledNumbers = shuffle(numbers);


// Erstellung API-Endpunkt (GET)
app.get('/', (req, res) => {

  // Response per Response-Objekt senden
  res.send(`Zufällige Zahlen: ${shuffledNumbers}`);
});


// Express.JS Instanz/Server starten. Callback-Funktion erzeugt Ausgabe in Konsole
app.listen(PORT, () => {
  console.log(`Server läuft auf Port ${PORT}`);
});
