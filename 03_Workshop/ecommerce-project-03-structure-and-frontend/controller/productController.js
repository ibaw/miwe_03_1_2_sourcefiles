import Product from "../classes/product.js";
import { loadProducts, save } from "../helper/product.js";


// Initialisieren der Produkte durch Laden der Inhalte aus der Datei
let products = loadProducts();


// Controller zur Verarbeitung des Abrufs aller Produkte
export const getProducts = (req, res) => {
  res.json(products);
};

// Controller zur Verarbeitung des Abrufs eines einzelnen Produkts nach ID
export const getProduct = (req, res) => {
  const found = products.find(element => element.id === req.params.id);

  if (found) {
    res.json(found);
  } else {
    res.status(404).json({
      success: false,
      message: `Kein Produkt mit der ID ${req.params.id} gefunden`
    });
  }
};

// Controller zur Verarbeitung der Erstellung eines neuen Produkts
export const createProduct = (req, res) => {
  const { name, price } = req.body;

  if (!name || price === undefined) { // Überprüfung der erforderlichen Felder
    return res.status(400).json({
      success: false,
      message: "Name und Preis sind erforderlich"
    });
  }

  const product = new Product(name, price);
  products.push(product);
  save();
  res.status(201).json({ success: true, data: product });
};

// Controller zur Verarbeitung der Aktualisierung eines Produkts nach ID
export const updateProduct = (req, res) => {
  let modified = false;
  products = products.map(element => {
    if (element.id === req.params.id) {
      modified = true;
      return { ...element, ...req.body, id: element.id }; // ID beibehalten
    } else {
      return element;
    }
  });

  if (modified) {
    save();
    res.json({ success: true, data: products });
  } else {
    res.status(404).json({
      success: false,
      message: `Kein Produkt mit der ID ${req.params.id} gefunden`
    });
  }
};

// Controller zur Verarbeitung der Löschung eines Produkts nach ID
export const deleteProduct = (req, res) => {
  const lengthBefore = products.length;
  products = products.filter(element => element.id !== req.params.id);

  if (products.length < lengthBefore) {
    save();
    res.json({ success: true, data: products });
  } else {
    res.status(404).json({
      success: false,
      message: `Kein Produkt mit der ID ${req.params.id} gefunden`
    });
  }
};
