import { v4 as uuidv4 } from "uuid"; // Importieren der Funktion uuidv4 zur Generierung von UUIDs

/**
 * Repräsentiert ein Produkt mit verschiedenen Attributen wie Abmessungen und Preis.
 * @class
 */
export default class Product {
  /**
   * Erstellt eine Instanz eines Produkts.
   * @param {string} name - Der Name des Produkts.
   * @param {number} preis - Der Preis des Produkts.
   * @param {number} gewicht - Das Gewicht des Produkts.
   * @param {number} laenge - Die Länge des Produkts.
   * @param {number} breite - Die Breite des Produkts.
   * @param {number} hoehe - Die Höhe des Produkts.
   */
  constructor(name, preis, gewicht = 0, laenge = 0, breite = 0, hoehe = 0) {
    if (!name || preis === undefined || preis < 0) {
      throw new Error("Name und ein positiver Preis sind erforderlich.");
    }
    this.id = uuidv4(); // Generieren einer eindeutigen ID für jedes Produkt mit uuidv4
    this.name = name;
    this.preis = preis;
    this.gewicht = gewicht;
    this.laenge = laenge;
    this.breite = breite;
    this.hoehe = hoehe;
  }
}
