import fs from "node:fs";
import path from "node:path";

// __dirname in ES6-Modulen definieren
import { fileURLToPath } from 'node:url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// Dateipfad für den Warenkorb, unter Berücksichtigung aller Betriebssysteme
const CART_FILEPATH = path.join(__dirname, '../data/cart.json');


// Funktion, um den Warenkorb aus einer Datei zu laden
export const loadCart = () => {
  try {
    const data = fs.readFileSync(CART_FILEPATH, "utf-8");
    return JSON.parse(data);
  } catch (error) {
    console.error("Fehler beim Laden des Warenkorbs:", error);
    return [];
  }
};


// Funktion zum Speichern des Warenkorbs in einer Datei
export const save = cart => {
  try {
    fs.writeFileSync(CART_FILEPATH, JSON.stringify(cart, null, 2));
  } catch (error) {
    console.error("Fehler beim Speichern des Warenkorbs:", error);
    throw new Error("Speichern des Warenkorbs fehlgeschlagen.");
  }
};