import fs from "node:fs";
import path from "node:path";

// __dirname in ES6-Modulen definieren
import { fileURLToPath } from 'node:url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// Dateipfad für den Warenkorb, unter Berücksichtigung aller Betriebssysteme
const PRODUCT_FILEPATH = path.join(__dirname, '../data/products.json');


// Funktion, um die Produkte aus einer Datei zu laden
export const loadProducts = () => {
  try {
    const data = fs.readFileSync(PRODUCT_FILEPATH, "utf-8");
    console.log(data)
    return JSON.parse(data);
  } catch (error) {
    console.error("Fehler beim Laden der Produkte:", error);
    return [];
  }
};


// Funktion zum Speichern der Produkte in einer Datei
export const save = () => {
  try {
    fs.writeFileSync(PRODUCT_FILEPATH, JSON.stringify(products, null, 2));
  } catch (error) {
    console.error("Fehler beim Speichern der Produkte:", error);
    throw new Error("Speichern der Produkte fehlgeschlagen.");
  }
};


// Funktion zum Finden eines Produkts anhand der ID
export const findProductById = (id) => {
  const products = loadProducts(); // Laden aller Produkte
  const product = products.find(product => product.id === id); // Suchen des Produkts nach ID

  if (product) {
    return product; // Rückgabe des gefundenen Produkts
  } else {
    console.error(`Produkt mit der ID ${id} nicht gefunden.`);
    return null; // NULL zurückgeben, wenn das Produkt nicht gefunden wird
  }
};