import express from "express"; // Express-Modul für Router
import {
  getProducts,
  getProduct,
  createProduct,
  updateProduct,
  deleteProduct
} from "../controller/productController.js"; // Importieren der Controller-Funktionen

const router = express.Router(); // Erstellen eines neuen Routers

// Definieren der Routen und Zuordnung zu den Controller-Funktionen
router.get("/", getProducts); // Route zum Abrufen aller Produkte
router.get("/:id", getProduct); // Route zum Abrufen eines einzelnen Produkts nach ID
router.post("/", createProduct); // Route zum Erstellen eines neuen Produkts
router.put("/:id", updateProduct); // Route zum Aktualisieren eines Produkts nach ID
router.delete("/:id", deleteProduct); // Route zum Löschen eines Produkts nach ID

export default router; // Exportieren des Routers zur Verwendung in der Hauptanwendung
