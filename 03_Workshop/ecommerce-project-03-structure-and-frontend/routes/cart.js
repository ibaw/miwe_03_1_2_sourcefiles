import express from "express"; // Importieren des Express-Moduls zum Erstellen von Routern
import { getCart, addToCart, removeFromCart } from "../controller/cartController.js"; // Importieren der Controller-Funktionen für den Warenkorb

const router = express.Router(); // Erstellen eines neuen Routers

// Definieren der Routen für den Warenkorb
router.get("/", getCart); // Route zum Abrufen des gesamten Warenkorbs
router.post("/", addToCart); // Route zum Hinzufügen eines Produkts zum Warenkorb
router.delete("/:id", removeFromCart)

export default router; // Exportieren des Routers zur Verwendung in der Hauptanwendung
