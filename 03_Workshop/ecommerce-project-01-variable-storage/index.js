import express from "express";

const app = express();
const PORT = process.env.PORT || 3001;

// Initiale Liste von Produkten, jeweils mit einer eindeutigen ID und einem Namen
let products = [
  {
    id: 1,
    name: "Produkt 1"
  },
  {
    id: 2,
    name: "Produkt 2"
  }
];

// Middleware zum Parsen eingehender JSON-Anfragen
app.use(express.json());

// Middleware für die Fehlbehandlung
app.use((err, req, res, next) => {
  console.error(err.stack); // Fehler in der Konsole ausgeben
  res.status(500).json({ error: "Ein interner Serverfehler ist aufgetreten." });
});

// Route zum Testen des Servers
app.get("/", (req, res) => {
  res.json("Hello World");
});

// Alle Produkte abrufen
app.get("/product", (req, res) => {
  res.json(products);
});

// Ein einzelnes Produkt nach ID abrufen
app.get("/product/:id", (req, res) => {
  // Produkt nach ID suchen
  const found = products.find(element => element.id == req.params.id);
  if (found) {
    res.json(found);
  } else {
    res.status(404).json({ error: "Produkt nicht gefunden" }); // Fehlerbehandlung: Produkt nicht gefunden
  }
});

// Ein neues Produkt hinzufügen
app.post("/product", (req, res) => {
  const { name } = req.body;

  // Überprüfen, ob der Name vorhanden ist
  if (!name) {
    return res.status(400).json({ error: "Name ist erforderlich" }); // Fehlerbehandlung: Fehlender Produktname
  }

  // Neues Produkt erstellen
  const newProduct = { id: products.length + 1, name };
  products.push(newProduct);
  
  res.status(201).json({ success: true, data: newProduct });
});

// Ein bestehendes Produkt nach ID aktualisieren
app.put("/product/:id", (req, res) => {
  let found = false;
  const { name } = req.body;

  // Überprüfen, ob der Name vorhanden ist
  if (!name) {
    return res.status(400).json({ error: "Name ist erforderlich" }); // Fehlerbehandlung: Fehlender Produktname
  }

  // Produkte aktualisieren
  let newProducts = products.map(element => {
    if (element.id == req.params.id) {
      found = true;
      return { id: element.id, name }; // Bestehende Produkt-ID beibehalten
    } else {
      return element;
    }
  });

  if (found) {
    products = newProducts;
    res.json({ success: true, data: products });
  } else {
    res.status(404).json({ error: "Produkt nicht gefunden" }); // Fehlerbehandlung: Produkt nicht gefunden
  }
});

// Ein Produkt nach ID löschen
app.delete("/product/:id", (req, res) => {
  const lengthBefore = products.length;

  // Produkt anhand der ID filtern und entfernen
  products = products.filter(element => element.id != req.params.id);
  
  if (products.length < lengthBefore) {
    res.json({ success: true, data: products });
  } else {
    res.status(404).json({ error: "Produkt nicht gefunden" }); // Fehlerbehandlung: Produkt nicht gefunden
  }
});

// Starten des Servers
app.listen(PORT, () => {
  console.log(`Server läuft auf Port ${PORT}`);
});
